# Contributopia : degoogliser ne suffit pas, contribuons à la transition

Installer reveal-md

```
npm i -g reveal-md
```

Générer le diaporama en pdf

```
reveal-md contributopia-transition.md --print
```
(en html, enlever l'option `--print`)

Générer le diaporama en html

```
reveal-md contributopia-transition.md --static public
```