---
theme: solarized
css: css/styles.css
---

# Contributopia 

## Dégoogliser ne suffit pas : contibuons à la transition !

---

# Dégoogliser ?

## D'où on vient…

Notes:

Militante pour l'utilisation de logiciels libres, puis plus largement intéressée par la question des Communs comme piste pour un monde futur, puisque celui-ci s'écroule. Je fais partie d'Alternatiba depuis 2017. Sinon je travaille dans le développement web.

----

## Le logiciel libre

Logiciel qui donne des libertés à l'utilisateurice

1. utiliser
1. copier
1. étudier
1. modifier et diffuser les copies

Ex: LibreOffice, VLC, Spip, … GNU/Linux (Ubuntu)

----

## L'art libre, les savoirs communs

- Même principe que les logiciels libres
- Licences *Créative Commons*, *ArtLibre*, …

Ex : [Wikipedia](http://wikipedia.org/), [OpenStreetMap](http://openstreetmap.org/), [OpenFoodFact](https://world.openfoodfacts.org), [Ekopedia](https://www.ekopedia.fr/), [TelaBotanica](http://tela-botanica.org/)…

Musique libre sur [Dogmazic](http://play.dogmazic.net/), peinture avec [Gwenn Simmel](www.gwennseemel.com/), BD avec [David Revoy](http://davidrevoy.com/), photos sur [Wikimedia Commons](http://commons.wikimedia.org/), livres chez [Framabook](http://framabook.org/)…

----

## Des assos et collectifs

- Aider le public à utiliser des logiciels libres : les groupes d'utilisateurs⋅ices de GNU/Linux comme le [CULTe](http://toulibre.org/) ou [Toulibre](https://www.culte.org/)
- Promouvoir le logiciel libre : l'[April](https://april.org/)
- Proposer l'accès à internet dans des zones blanches ou grises : la Fédération des Fournisseurs d'Accès Internet  (FAI) associatifs, la [FFDN](http://ffdn.org/)
- Défendre nos libertés à l'ère du numérique : la [Quadrature du Net](https://www.laquadrature.net/)
- … et [Framasoft](http://framasoft.org/), logiciel libre et éducation populaire

----

## Framasoft c'est…

*Depuis 2001*

- annuaire du libre, maison d'édition, hébergeur de services en ligne alternatifs aux géants du WEB, blog avec de nombreux articles traduits…
- une association, 8 salarié⋅e⋅s et 35 bénévoles membres, des centaines d'autres sur de nombreux projets
-  financée à 90% avec vos dons, 7 secondes du budget annuel de Alphabet-Google = 200 000€ (encore moins à ses début)

Notes:

ajouter l'infographie de Geoffrey Dorne

----

## Surtout : une bande de potes

----

## Dégooglisons-internet.org

*2014 – 2017*

- 31 services alternatifs aux géants du web
- 7 serveurs (chez Hertzner, hébergeur Allemand)
- 3 ans de folie pour les salarié⋅e⋅s et les bénévoles

----

… pour des milliers d'utilisateurices

![Framastats 242000 pads](images/framastat-pad-2018-0921.png)

Notes:

Ex : chiffres des pads
https://twitter.com/framasoft/status/1043220820552646656
242 000 pads actifs

----

## Mais c'est une goutte d'eau !

----

## Essaimage avec le C.H.A.T.O.N.S.

Collectif des Hebergeurs Alternatifs Transparents Ouverts Neutres et Solidaires

→ Les AMAPs des logiciels libres

Quelques membres : Le PIC à Ramonville, les Colibris, Animafac…

Notes:

Un peu comme les AMAP : mettre en relation directe entre les fournisseurs et les utilisateurs.

Retrouvez le PIC ici-même

----

![Rétroacronyme](./images/tsq_020_l_accro_d_nimes-pyg.png)

----

## Et maintenant ?

### Dégoogliser ne suffit pas.

Composter ne suffit pas.
Donner ne suffit pas.
Faire sa part ne suffit pas.
Manifester ne suffit pas.
Bénévoler ne suffit pas.
Être vegan ne suffit pas.

---

# Le monde dont nous ne voulons plus

----

## *Avertissement*

Constats et cheminement politique plus personnels qu'une position officielle de Framasoft.

Notes:

Un avis assez personnel, partagé par certains membres de l'asso

----

## Anthropocène

L'activité humaine, cause de changements à l'échelle géologique.

Activité humaine ?
Changement climatique ?
Et les questions sociales ?

Notes:

Activité humaine ? notre système économique, la recherche du profit, l'extractivisme, la croissance…

Changement climatique ? Pollutions, disparition de la biodiversité, dérèglements climatiques

Mais pas que : inégalités croissantes, colonialisme économique…

(s'inspirer du slide débat mouvant puis philo de Pyg)

----

## Impact écologique du numérique

Extraction des Terres rares, dépenses d'énergie, déchets électroniques

![Déchets électroniques](images/Computer_that's_had_its_chips_-_geograph.org.uk_-_735712.jpg)

Notes:

Nous en sommes tous conscients ici : les terres rares, l'énergie des serveurs, les déchets électroniques…

Solutions ? la récup', conserver son téléphone

----

## Impact économique et social

Exploitation des travailleur⋅e⋅s : fabrication, distribution, plateformes…

[![Manifestation Deliveroo](images/827015-ez-manifestation-coursiers-deliveroo-770x433.jpg)](https://www.lyoncapitale.fr/actualite/deliveroo-uber-les-livreurs-a-velo-en-greve-pour-la-coupe-du-monde/)

Notes:

Dans une économie mondialisée, exploitation des travailleurs à tous les niveaux : fabrication, distribution, traitement des déchets, 

Une économie quaternaire ? travailleurs individualisés par l'ubérisation

----

## Publicité ciblée

Des technologies conçues pour surveiller nos comportements, afin de nous pousser à consommer.

> « Le but principal de toutes les dernières innovations technologiques dans le numérique est de vous faire cliquer sur de la publicité » 

 – *Zeynep Tufekci, auteure de* Twitter and Teargas

----

## Capitalisme de surveillance

Mieux cibler la publicité, pour mieux nous inciter à consommer. 

### Conséquences

Auto-censure, fabrique du consentement, apauvrissement diversité culturelle, utilisation surveillance à des fins politiques

Notes:

Tout notre système économique est basé sur ce principe.  Ce monde dont nous ne voulons plus.

----

##  Effondrement

Éviter l'effondrement ? plutôt s'y préparer. 

Peu d'illusion sur l'avenir du numérique

### En attendant

Le numérique pour organiser la transition : communiquer, s'organiser, échanger des savoirs…

Toute technologie = *pharmakon* (Stiegler)

Notes:

On est prêts pour la fin des Internets : Numahell apprend à jardiner et à construire des meubles, et Pouhiou tricote

En attendant, on a besoin de communiquer, s'organiser, partager… Alors nous invoquons **Stiegler** et son *Pharmakon* : nous proposons des alternatives.

---

# Numérique en transition

----

## Faire. 
### Faire sans eux. Faire malgré eux.

> « On vit ici, on habite ici, on reste ici. Mais le temps est venu d’assembler tous ces petits bouts de liberté, d’autonomie que nous avons su construire, conquérir ou préserver, de déclarer l’autonomie de ce patchwork, et de faire simplement, calmement, sécession. »

– *Lt Francesco Casabaldi,* « Vers une fédération de communes libres »

Notes:

On ne boit plus le thé à l'éducation nationale : inefficace

Texte complet :

> « Alors puisque décidément les vies qu’on nous propose sont trop étroites pour y loger nos rêves, puisque décidément l’organisation politique, économique et sociale de ce(s) pays nous est chaque jour plus insupportable, puisque même l’air ambiant sent tellement le fascisme rampant et les gaz lacrymogènes qu’il en est devenu irrespirable, puisque rester ici un jour de plus, c’est finalement continuer à collaborer à l’exploitation et à la destruction de la planète et de l’humanité, puisque les libertés qu’on nous jette sous la table sont trop maigres pour notre dignité, puisque enfin, nous sommes encore vivants et debout, et bien, partons.
> Entendons nous bien. On vit ici, on habite ici, on reste ici. Mais le temps est venu d’assembler tous ces petits bouts de liberté, d’autonomie que nous avons su construire, conquérir ou préserver, de déclarer l’autonomie de ce patchwork, et de faire simplement, calmement, sécession. »
> – *Lt Francesco Casabaldi,* « Vers une fédération de communes libres »

----

## Comment

- Nos valeurs : 
    - Bienveillance, émancipation, inclusivité, partage
- Nos moyens : 
    - Association loi 1901, ≤ 10 salariés, bande de potes, humour et bonne humeur, dons diversifiés, alliances

Notes:

(oui, on ne s’adresse plus aux 1 ers de cordée de la startup nation qui roulent en 4x4)

----

## Nous arrivons !

* L'éducation populaire (depuis longtemps)
* L'économie solidaire et sociale
* Les milieux militants de l'écologie politique, LGBT+, anti-racisme

Pas dans les partis politiques

Notes:

Nous essayons de convaincre nos camarades de nous rejoindre

----

## Contributopia : 2018-2020

« Outiller numériquement la société de contribution »

![Illustration Contributopia](./images/0_Main-menu_by-David-Revoy.jpg)

----

### 2018 – Améliorer et proposer des outils

Améliorer les outils libres pour les besoins des transitions écologiques, politiques, sociales…

- Frama.site → créer votre site internet
- Peertube → héberger vos vidéos

À venir : Framapétitions, Framameet

----

### 2019 – S'émanciper

« Sofware To The People »
    
Aider à l'émancipation de chacun⋅e vis à vis des technos numériques

- C.H.A.T.O.N.S → collectif d'hébergeurs web éthiques 
- Yunohost → pour s'auto-héberger facilement
- Essaimer hors de France
- Soutenir la contribution : s'émanciper des géants du web qui financent les logiciels libres 

----

### 2020 – Transmettre les savoir-faire, imaginer les possibles

Actions de médiation,  [contrib'ateliers](http://contribateliers.frama.site/), création d'un MOOC et d'autres contenus pédagogiques… et pourquoi pas une Université Populaire du Libre, Ouverte, Accessible et Décentralisée

Proposer des outils pour co-construire des savoirs ou d’autres communs contributifs .

Imaginer le monde que nous voulons.

----

## Merci !

Et retrouvez Framasoft, le PIC et le CULTe sur les stands

---

# Credits

* Extrait de BD "L'acroc de Nîmes" sur le [blog Grisebouille de Gee](https://grisebouille.net/laccro-dnimes/) – [CC-By-SA]()

* Déchets électroniques par [Richard Dorrell / Computer that's had its chips](https://commons.wikimedia.org/wiki/File:Computer_that%27s_had_its_chips_-_geograph.org.uk_-_735712.jpg) –  [CC BY-SA 2.0]()

* Cyclistes Deliveroo en manif par [LyonCapitale](https://www.lyoncapitale.fr/actualite/deliveroo-uber-les-livreurs-a-velo-en-greve-pour-la-coupe-du-monde/)

* Illustration de Contributopia par [David Revoy]() – [CC-By 4.0]()